import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  basketId: '',
  isAuth: false,
  isLoading: false,
  user: {},
  users: [],
  userInfo: {}
}

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setIsAuth: (state, action) => {
      state.isAuth = action.payload;
    },
    switchToPublic: (state) => {
      state.isAuth = false;
    },
    setLoading: (state, action) => {
      state.isLoading = action.payload;
    },
    setUser: (state, action) => {
      state.user = action.payload;
    },
    setUsers: (state, action) => {
      state.users = action.payload;
    },
    setUserBasket: (state, action) => {
      state.basketId = action.payload;
    },
    setUserInfo: (state, action) => {
      state.userInfo = action.payload;
    }
  },
})

export const { setIsAuth, switchToPublic, setLoading, setUser, setUsers, setUserBasket, setUserInfo } = userSlice.actions
export default userSlice.reducer
