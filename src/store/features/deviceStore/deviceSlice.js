// Modules
import { createSlice } from '@reduxjs/toolkit';

// Utils
import { BRAND_MODAL, DEVICE_MODAL, TYPE_MODAL } from '../../../utils/constants';

const initialState = {
  types: [],
  brands: [],
  devices: [],
  basketDevices: {},
  selectedType: {},
  selectedBrand: {},
  selectedDevice: {},
  isBrandModalOpen: false,
  isDeviceModalOpen: false,
  isTypeModalOpen: false,
  isToastWarning: false,
  isToastSuccess: false,
  page: 1,
  totalCountPage: 0,
  limit: 4
}

export const deviceSlice = createSlice({
  name: 'device',
  initialState,
  reducers: {
    setBrands: (state, action) => {
      state.brands = action.payload;
    },
    setDevices: (state, action) => {
      state.devices = action.payload;
    },
    setTypes: (state, action) => {
      state.types = action.payload;
    },
    setSelectedType: (state, action) => {
      state.selectedType = action.payload;
      state.page = 1;
    },
    setSelectedBrand: (state, action) => {
      state.selectedBrand = action.payload;
    },
    setSelectedDevice: (state, action) => {
      state.selectedDevice = action.payload;
    },
    setPage: (state, action) => {
      state.page = action.payload;
    },
    setTotalCountPage: (state, action) => {
      state.totalCountPage = action.payload;
    },
    setBasketDevices: (state, action) => {
      state.basketDevices = action.payload;
    },
    setIsToastWarning: (state, action) => {
      state.isToastWarning = action.payload;
    },
    setIsToastSuccess: (state, action) => {
      state.isToastSuccess = action.payload;
    },
    openModal: (state, action) => {
      if (action.payload === BRAND_MODAL) {
        state.isBrandModalOpen = true;
      }
      if (action.payload === DEVICE_MODAL) {
        state.isDeviceModalOpen = true;
      }
      if (action.payload === TYPE_MODAL) {
        state.isTypeModalOpen = true;
      }
    },
    closeModal: (state) => {
      state.isBrandModalOpen = false;
      state.isDeviceModalOpen = false;
      state.isTypeModalOpen = false;
    }
  },
})

export const { 
  setBasketDevices,
  setBrands,
  setDevices,
  setTypes,
  setSelectedType, 
  setSelectedBrand, 
  setSelectedDevice, 
  setPage,
  setTotalCountPage,
  setIsToastWarning,
  setIsToastSuccess,
  openModal, 
  closeModal 
} = deviceSlice.actions
export default deviceSlice.reducer
