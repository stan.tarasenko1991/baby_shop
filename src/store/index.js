// Modules
import { configureStore } from '@reduxjs/toolkit';

// Slices
import deviceReducer from './features/deviceStore/deviceSlice';
import userReducer from './features/userStore/userSlice';

export const store = configureStore({
  reducer: {
    device: deviceReducer,
    user: userReducer,
  },
})
