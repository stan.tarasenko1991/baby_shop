// Modules
import { useDispatch, useSelector } from 'react-redux';
import { openModal, closeModal } from '../../store/features/deviceStore/deviceSlice';
import { useNavigate } from 'react-router-dom';
import { Button } from 'react-bootstrap';

// Components
import BrandModal from '../../components/modals/BrandModal/BrandModal';
import DeviceModal from '../../components/modals/DeviceModal';
import TypeModal from '../../components/modals/TypeModal';

// Utils
import { BRAND_MODAL, DEVICE_MODAL, TYPE_MODAL, USER_LIST_ROUTE } from '../../utils/constants';

// Styles
import './admin-styles.scss';

const Admin = () => {
  const { isBrandModalOpen, isDeviceModalOpen, isTypeModalOpen } = useSelector((state) => state.device);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const showModal = (id) => {
    dispatch(openModal(id))
  }

  const hideModal = () => {
    dispatch(closeModal())
  };

  const goToUserList = (route) => {
    navigate(route)
  }

  return (
    <div className="admin-main">
      <button className="admin-main_button" onClick={() => {showModal(TYPE_MODAL)}}>Add/Delete type</button>
      <button className="admin-main_button" onClick={() => {showModal(BRAND_MODAL)}}>Add/Delete brand</button>
      <button className="admin-main_button" onClick={() => {showModal(DEVICE_MODAL)}}>Add device</button>
      <Button 
        className="admin-main_userList" 
        onClick={() => {goToUserList(USER_LIST_ROUTE)}} 
        variant='primary'>
          Users List
      </Button>
      <BrandModal show={isBrandModalOpen} onHide={hideModal} />
      <DeviceModal show={isDeviceModalOpen} onHide={hideModal} />      
      <TypeModal show={isTypeModalOpen} onHide={hideModal} />
    </div>
  );
};

export default Admin;
