// Modules
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Spinner } from 'react-bootstrap';
import { fetchBasket } from '../../http/userAPI';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { NavLink, useLocation, useNavigate } from 'react-router-dom';
import { LOGIN_ROUTE, REGISTRATION_ROUTE, SHOP_ROUTE } from '../../utils/constants';
import { login, registration } from '../../http/userAPI';
import { setIsAuth, setLoading, setUser, setUserBasket } from '../../store/features/userStore/userSlice';
// import { check } from '../../http/userAPI';

// Styles
import './auth-styles.scss';

const Auth = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const dispatch = useDispatch();
  const isLogin = location.pathname === LOGIN_ROUTE;

  const user = useSelector((state) => state.user);
  const { isLoading } = user;

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const signIn = async (email, password) => {
    try {
      if (isLogin) {
        const response = await login(email, password);
        dispatch(setIsAuth(true))
        dispatch(setLoading(true))
        sessionStorage.setItem('role', response.role)
        dispatch(setUser(response))
        fetchBasket(response.id).then(data => {
          const {id} = data.data;
          sessionStorage.setItem('basketId', id)
          dispatch(setUserBasket(data.data))
        })
        setTimeout(() => {
          dispatch(setLoading(false))
          navigate(SHOP_ROUTE)
          return response
        }, 1000)
      } else {
        const response = await registration(email, password);
        sessionStorage.setItem('role', response.role)
        dispatch(setIsAuth(true))
        dispatch(setLoading(true))
        dispatch(setUser(response))
        fetchBasket(response.id).then(data => {
          const {id} = data.data;
          sessionStorage.setItem('basketId', id)
          dispatch(setUserBasket(data.data))
        })
        setTimeout(() => {
          dispatch(setLoading(false))
          navigate(SHOP_ROUTE)
          return response
        }, 1000)
      }
    } catch (e) {
      alert(e.response.data.message)
    }
  };

  return (
    <div className="auth_main">
      <div className="auth_main_form">
        <Form>
        {isLoading 
          ? <div className="mb-3 d-flex justify-content-center">
              <Spinner animation={"grow"} />
            </div>
          : <h3 className="mb-3 d-flex justify-content-center">
            {isLogin ? 'Log In' : 'Registration'}
            </h3>
        }  
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            onChange={e => setEmail(e.target.value)} 
            type="email" 
            placeholder="Enter email" 
            value={email} 
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control 
            onChange={e => setPassword(e.target.value)}
            type="password" 
            placeholder="Password" 
            value={password} 
          />
        </Form.Group>

        <div className="auth_main_form_btn">
          {isLogin 
          ? <div>
              <span className="auth_main_form_btn_info">Don`t have an account?</span>
              <NavLink to={REGISTRATION_ROUTE}>Registration</NavLink>
            </div> 
          : <div>
              <span className="auth_main_form_btn_info">Have an account?</span>
              <NavLink to={LOGIN_ROUTE}>Log In</NavLink>
            </div>
          }
          <Button variant="outline-success" onClick={() => signIn(email, password)}>
            Submit
          </Button>
        </div>
        </Form>
      </div>
    </div>
  );
};

export default Auth;
