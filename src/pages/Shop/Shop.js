// Modules
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Col, Container, Row } from 'react-bootstrap';
import Toast from 'react-bootstrap/Toast';

// API
import { fetchBrands, fetchDevices, fetchTypes } from '../../http/deviceAPI';
import { setBasketDevices, setBrands, setDevices, setIsToastSuccess, setTotalCountPage, setTypes } from '../../store/features/deviceStore/deviceSlice';
import { fetchAllBasketDevices, fetchUsers } from '../../http/userAPI';
import { setUsers } from '../../store/features/userStore/userSlice';
import { setIsToastWarning } from '../../store/features/deviceStore/deviceSlice';

// Components
import BrandBar from '../../components/BrandBar';
import DeviceList from '../../components/DeviceList';
import TypeBar from '../../components/TypeBar';
import Pages from '../../components/Pages';
import { ADMIN_ROLE } from '../../utils/constants';

const Shop = () => {
const dispatch = useDispatch();
const data = useSelector((state) => state.device);
const { selectedBrand, 
        selectedType, 
        page, 
        limit, 
        isToastWarning, 
        isToastSuccess 
      } = data;

const role = sessionStorage.getItem('role');
const basketId = Number(sessionStorage.getItem('basketId'));

useEffect(() => {
  fetchTypes().then(data => {
    dispatch(setTypes(data))
  })
  fetchBrands().then(data => {
    dispatch(setBrands(data))
  })
  fetchDevices(null, null, 1, 4).then(data => {
    dispatch(setDevices(data.rows))
    dispatch(setTotalCountPage(data.count))
  })
}, [dispatch]);

useEffect(() => {
    fetchAllBasketDevices(basketId).then(data => {
      dispatch(setBasketDevices(data))
      sessionStorage.setItem('basketItemsCount', data.count)
    })
}, [dispatch, basketId]);

useEffect(() => {
  if (role === ADMIN_ROLE)
  fetchUsers().then(data => {
    dispatch(setUsers(data.data))
  });
}, [dispatch, role]);

useEffect(() => {
  fetchDevices(selectedType.id, selectedBrand.id, page, limit).then(data => {
    dispatch(setDevices(data.rows))
    dispatch(setTotalCountPage(data.count))
  })
}, [dispatch, selectedType, selectedBrand, page, limit]);

  return (
    <Container className='m-0'>
      <Row>
        <Col md={3} style={{
          backgroundImage: 'linear-gradient(to right, white, white, lightblue)', 
          height: '100vh',
          position: 'fixed'
          }}>
          <TypeBar />
        </Col>
        <Col md={9} style={{marginLeft: '30%'}}>
          <BrandBar />
          <DeviceList />
          <Pages />
        </Col>
        <Toast 
            style={{position: 'absolute', right: '20px', marginTop: '50px'}}
            onClose={() => dispatch(setIsToastWarning(false))} 
            show={isToastWarning} 
            delay={3000} 
            autohide 
            bg='warning'>
          <Toast.Body>This device already in your basket</Toast.Body>
        </Toast>
        <Toast 
            style={{position: 'absolute', right: '20px', marginTop: '50px'}}
            onClose={() => dispatch(setIsToastSuccess(false))} 
            show={isToastSuccess} 
            delay={3000} 
            autohide 
            bg='success'>
          <Toast.Body>This device added in your basket</Toast.Body>
        </Toast>
      </Row>
    </Container>
  );
};

export default Shop;
