// Modules
import { Button, Card, Col, Container, Image, Row } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useEffect } from 'react';
import { fetchOneDevice } from '../../http/deviceAPI';
import { setBasketDevices, setIsToastSuccess, setIsToastWarning, setSelectedDevice } from '../../store/features/deviceStore/deviceSlice';
import { useDispatch } from 'react-redux';

// API
import { addDeviceToBasket, fetchAllBasketDevices } from '../../http/userAPI';

// Assets
import star from '../../assets/icons/bigStar.svg';
import { REACT_APP_API_URL } from '../../constants';

// Styles
import './device-page-styles.scss';

const DevicePage = () => {
const {id} = useParams();
const dispatch = useDispatch();
const deviceData = useSelector((state) => state.device);
const basketId = Number(sessionStorage.getItem('basketId'));
const { selectedDevice, basketDevices } = deviceData;

useEffect(() => {
  if (basketId) {
    fetchAllBasketDevices(basketId).then(data => {
      dispatch(setBasketDevices(data))
    })
  }
}, [dispatch, basketId]);

useEffect(() => {
  fetchOneDevice(id).then(data => {
    dispatch(setSelectedDevice(data))
  })
}, [dispatch, id]);

const addToBasket = (deviceId) => {
    let hasMatch = basketDevices.rows.filter(i => i.deviceId === deviceId).length > 0;
    if (hasMatch) {
      setIsToastWarning(true)
    } else {
      addDeviceToBasket(basketId, deviceId).then(data => {
        fetchAllBasketDevices(basketId).then(data => {
          dispatch(setBasketDevices(data))
        })
        setIsToastSuccess(true)
      })
    }
};

  return (
    <div className='containerMain'>
    <Container className='d-flex'>
      <Col md={4}>
        {selectedDevice.img && 
          <Image 
            height={300} 
            src={REACT_APP_API_URL + '/' + selectedDevice.img} 
            width={300} 
          />
        }
      </Col>
      <Col md={4}>
        <Row className='justify-content-center'>
          <div className='device_title'>{selectedDevice.name}</div>
          <div 
            className='device_rating' 
            style={{
              background: `url(${star}) no-repeat center`,
              width: 240,
              height: 240
            }}>
            {selectedDevice.rating}
          </div>
        </Row>
      </Col>
      <Col md={4}>
        <Card 
          className='d-flex flex-column align-items-center justify-content-around'
          style={{width: 300, height: 300, border: '5px solid lightgrey'}}>
          <h3>{selectedDevice.price} UAH</h3>
          <Button variant='outline-dark' onClick={() => {addToBasket(selectedDevice.id)}}>Add to basket</Button>
        </Card>
      </Col>
    </Container>
    <Container className='mt-3'>
    <h1>Description</h1>  
    <Row style={{fontSize: '20px'}}>
        {selectedDevice.info && selectedDevice.info.map((data, index) => 
          <div key={data.id} style={{background: index % 2 === 0 ? 'lightgrey' : '', padding: '5px'}}>
            {data.title}: {data.description}
          </div>
        )}
      </Row>
    </Container>
    </div>
  );
};

export default DevicePage;
