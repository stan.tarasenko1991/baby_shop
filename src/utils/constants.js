export const ADMIN_ROUTE = '/admin';
export const LOGIN_ROUTE = '/login';
export const REGISTRATION_ROUTE = '/registration';
export const SHOP_ROUTE = '/';
export const BASKET_ROUTE = '/basket';
export const DEVICE_ROUTE = '/device';
export const USER_LIST_ROUTE = '/user_list';

// Modals Titles
export const BRAND_MODAL = 'brand';
export const DEVICE_MODAL = 'device';
export const TYPE_MODAL = 'type';

export const ADMIN_ROLE = 'ADMIN';
export const USER_ROLE = 'USER';

// Delete Buttons
export const DELETE_BUTTON_VALUES = [
  { name: 'Adding', value: 'add' },
  { name: 'Remove', value: 'delete' },
];
