// Modules
import { $authHost, $host } from './index';
import jwt_decode from 'jwt-decode';

export const registration = async (email, password) => {
  const { data } = await $host.post('api/user/registration', {email, password, role: 'USER'});
  const info = jwt_decode(data.jsonwebtoken);
  sessionStorage.setItem('token', data.jsonwebtoken);
  sessionStorage.setItem('email', info.email);
  return info;
};

export const login = async (email, password) => {
  const { data } = await $host.post('api/user/login', {email, password});
  const info = jwt_decode(data.jsonwebtoken);
  sessionStorage.setItem('token', data.jsonwebtoken);
  sessionStorage.setItem('email', info.email);
  return info;
};

export const check = async () => {
    const data = await $authHost.get('api/user/auth');
    return data;
};

export const fetchUsers = async () => {
  const data = await $authHost.get('api/user');
  return data;
};

export const deleteOneUser = async (id) => {
  const { data } = await $authHost.delete(`api/user/${id}`);
  return data;
};

// Basket
export const fetchBasket = async (userId) => {
  const data = await $host.get(`api/basket/${userId}`);
  return data;
};

export const addDeviceToBasket = async (basketId, deviceId) => {
  const data = await $host.post(`api/basket_devices`, {basketId, deviceId});
  return data;
};

export const fetchAllBasketDevices = async (basketId) => {
  const data = await $host.get(`api/basket_devices/${basketId}`);
  return data.data;
}
