// Modules
import { useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { useDispatch } from 'react-redux';

// Api
import { check } from './http/userAPI';
import { setIsAuth, setUser, setUserBasket } from './store/features/userStore/userSlice';

// Components
import AppRouter from './components/AppRouter';
import NavBar from './components/Navbar';

function App() {  
  const dispatch = useDispatch();
  const storageData = sessionStorage.length;
 
  useEffect(() => {
    if (storageData > 0) {
      check().then(data => {
        dispatch(setIsAuth(true))
        dispatch(setUser({ 
          email: sessionStorage.getItem('email'),
          role: sessionStorage.getItem('role')
        }))
        dispatch(setUserBasket(sessionStorage.getItem('basketId')))
       });
    }
  }, [dispatch, storageData]);

  return (
    <BrowserRouter>
      <NavBar />
      <AppRouter />
    </BrowserRouter>
  );
}

export default App;
