// Components
import Admin from './pages/Admin';
import BasketList from './components/BasketList';
import Shop from './pages/Shop';
import Auth from './pages/Auth';
import DevicePage from './pages/DevicePage';
import UsersList from './components/UsersList';

// Constants
import {
  ADMIN_ROUTE, 
  BASKET_ROUTE, 
  SHOP_ROUTE, 
  LOGIN_ROUTE, 
  REGISTRATION_ROUTE, 
  DEVICE_ROUTE,
  USER_LIST_ROUTE 
} from './utils/constants';

export const authRoutes = [
  {
    path: ADMIN_ROUTE,
    Component: <Admin />
  },
  {
    path: BASKET_ROUTE,
    Component: <BasketList />
  },
  {
    path: USER_LIST_ROUTE,
    Component: <UsersList />
  }
];

export const publicRoutes = [
  {
    path: SHOP_ROUTE,
    Component: <Shop />
  },
  {
    path: LOGIN_ROUTE,
    Component: <Auth />
  },
  {
    path: REGISTRATION_ROUTE,
    Component: <Auth />
  },
  {
    path: DEVICE_ROUTE + '/:id',
    Component: <DevicePage />
  }
];
