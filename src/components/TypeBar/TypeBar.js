// Modules
import { useDispatch, useSelector } from 'react-redux';
import { Button } from 'react-bootstrap';
import ListGroup from 'react-bootstrap/ListGroup';
import { 
  setDevices, 
  setPage, 
  setSelectedBrand, 
  setSelectedType, 
  setTotalCountPage 
} from '../../store/features/deviceStore/deviceSlice';
import { fetchDevices } from '../../http/deviceAPI';

// Styles
import './typeBar-styles.scss';

const TypeBar = () => {
  const devices = useSelector((state) => state.device);
  const { types, selectedType } = devices;
  const dispatch = useDispatch();

  const selectType = (type) => {
    dispatch(setSelectedType(type))
  }

  const clearFilters = () => {
    fetchDevices(null, null, 1, 4).then(data => {
      dispatch(setDevices(data.rows))
      dispatch(setTotalCountPage(data.count))
      dispatch(setPage(1))
      dispatch(setSelectedType({}))
      dispatch(setSelectedBrand({}))
    })
  };

  return (
    <>
      <ListGroup style={{marginTop: '50px'}}>
      {types.map((type) => 
        <ListGroup.Item 
          active={type.id === selectedType.id}
          key={type.id} 
          onClick={() => selectType(type)}
          style={{ 
            border: 'none',
            cursor: 'pointer', 
            backgroundImage: type.id === selectedType.id 
              ? 'linear-gradient(to right, red, blue)' 
              : 'linear-gradient(to right, dodgerblue, blue)', 
            fontWeight: 'bold',
            color: 'white',
            textShadow: '1px 1px black' 
          }}
          className="card"
          >
            {type.name}
          </ListGroup.Item>
      )}
    </ListGroup>
    <Button 
      className='mt-4 m-2 p-2' 
      onClick={clearFilters}
      style={{ 
        border: 'none',
        cursor: 'pointer', 
        backgroundImage: 'linear-gradient(to right, lightslategrey, grey)', 
        fontWeight: 'bold',
        color: 'white',
        textShadow: '1px 1px black' 
      }}
    >
        Clear All Filters
    </Button>
    <div></div>
    </>
  );
};

export default TypeBar;
