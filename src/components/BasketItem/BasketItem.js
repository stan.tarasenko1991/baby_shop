// Modules
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Button, Image, Spinner } from 'react-bootstrap';
import { fetchOneDevice } from '../../http/deviceAPI';
import { REACT_APP_API_URL } from '../../constants';
import OrderModal from '../modals/OrderModal/OrderModal';

// Styles
import './basketItem-styles.scss';

const BasketItem = (props) => {
  const { id } = props;
  const { userInfo } = useSelector((state) => state.user);
  const [device, setDevice] = useState({});
  const [modalShow, setModalShow] = useState(false);

  const orderDevice = (deviceId) => {
    setModalShow(true);
  };
  const deleteDevice = (deviceId) => {
    console.log('deleteDevice', deviceId);
  };

  useEffect(() => {
    if (id) {
      fetchOneDevice(id).then(data => {
        setDevice(data);
      })
    }
  }, [id]);

  useEffect(() => {
    if (Object.keys(userInfo).length > 0) {
      setModalShow(false);
    }
  }, [userInfo]);

  return (
    <div className='basketItem'>
      <div className='basketItem_img'>
        {device.img 
          ? <Image src={REACT_APP_API_URL + '/' + device.img} height={60} width={60} />
          : <Spinner animation={"grow"} style={{margin: '12px'}} />
        }
      </div>
      <div className='basketItem_title'>
        {device.name}
      </div>
      <div className='basketItem_title'>
        {device.price} UAH
      </div>
      <div className='basketItem_order'>
        <Button variant='success' onClick={() => orderDevice(device.id)} style={{width: '120px'}}>Order</Button>
      </div>
      <div className='basketItem_clear'>
        <Button variant='secondary' onClick={() => deleteDevice(device.id)} style={{width: '120px'}}>Clear</Button>
      </div>
      <OrderModal 
        show={modalShow}
        onHide={() => setModalShow(false)}
        device={device}
      />
    </div>
  );
};

export default BasketItem;
