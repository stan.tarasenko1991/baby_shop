// Modules
import { useSelector } from 'react-redux';
import { Routes, Route } from 'react-router-dom';
import { authRoutes, publicRoutes } from '../routes';

// Pages
import Shop from '../pages/Shop';

const AppRouter = () => {
  const user = useSelector((state) => state.user);
  const { isAuth } = user;

  return (
    <>
    <Routes>
      {isAuth && authRoutes.map(({ path, Component }) => 
          <Route key={path} path={path} element={Component} />
        )}
      {publicRoutes.map(({ path, Component }) => 
          <Route key={path} path={path} element={Component} />
        )}  

        {/* Last Route for redirect if url is wrong  */}
        <Route path="*" element={<Shop />} />
    </Routes>
    </>
  );
};

export default AppRouter;
