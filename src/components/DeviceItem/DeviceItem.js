// Modules
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Image from 'react-bootstrap/Image';
import { ADMIN_ROLE, DEVICE_ROUTE } from '../../utils/constants';

// Assets
import { Button } from 'react-bootstrap';
import star from '../../assets/icons/star.svg';
import { REACT_APP_API_URL } from '../../constants';

// API
import { 
  deleteOneDevice, 
  fetchDevices 
} from '../../http/deviceAPI';
import { 
  setBasketDevices, 
  setDevices, 
  setIsToastSuccess, 
  setIsToastWarning, 
  setTotalCountPage 
} from '../../store/features/deviceStore/deviceSlice';
import { 
  addDeviceToBasket, 
  fetchAllBasketDevices 
} from '../../http/userAPI';

// Styles
import './device-item-styles.scss';

const DeviceItem = ({ device }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const {basketDevices} = useSelector((state) => state.device);
  const role = sessionStorage.getItem('role');

  const [isDelete, setIsDelete] = useState(false);

  const selectDevice = (device) => {
    navigate(DEVICE_ROUTE + '/' + device.id)
  }

  const deleteDevice = (id) => {
    setIsDelete(true)
    deleteOneDevice(id).then(data => {
      fetchDevices(null, null, 1, 4).then(data => {
        dispatch(setDevices(data.rows))
        dispatch(setTotalCountPage(data.count))
      })
    })
  }

  const addToBasket = (deviceId) => {
    const basketId = Number(sessionStorage.getItem('basketId'))
    let hasMatch = basketDevices.rows.filter(i => i.deviceId === deviceId).length > 0;
    if (hasMatch) {
      dispatch(setIsToastWarning(true));
    } else {
      addDeviceToBasket(basketId, deviceId).then(data => {
        fetchAllBasketDevices(basketId).then(data => {
          dispatch(setBasketDevices(data))
        })
        dispatch(setIsToastSuccess(true));
      })
    }
  };
  
  return (
    <div className="device_card" style={{display: isDelete ? 'none' : ''}}>
      <div className="device_card_box" onClick={() => selectDevice(device)}>
        <Image src={REACT_APP_API_URL + '/' + device.img} height={150} width={150} />
        <div className="device_card_title">{device.name}</div>
        <span className="device_card_price">Price: {device.price} UAH</span>
        <span className="device_card_rating">
          Rating: {device.rating} 
          <Image src={star} height={20} width={20} />
        </span>
        </div>
      {role === ADMIN_ROLE 
        ? <Button className='mb-2' onClick={() => deleteDevice(device.id)} variant='danger'>
            Delete
          </Button>
        : <Button className='mb-2' onClick={() => addToBasket(device.id)} variant='info'>
            Add to basket
          </Button>
      }
    </div>
  );
};

export default DeviceItem;
