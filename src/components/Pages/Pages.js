// Modules
import { Pagination } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';

// Store
import { setPage } from '../../store/features/deviceStore/deviceSlice';

const Pages = () => {
  const dispatch = useDispatch();
  const devices = useSelector((state) => state.device);
  const pageCount = Math.ceil(devices.totalCountPage / devices.limit);
  const pages = [];

  for (let i = 0; i < pageCount; i++) {
    pages.push(i + 1);
  }
  

  return (
    <Pagination className='mt-5'>
      {pages.map((page, idx) => 
        <Pagination.Item 
          active={devices.page === page} 
          key={idx}
          onClick={() => dispatch(setPage(page))}
        >
          {page}
        </Pagination.Item>
      )}
    </Pagination>
  );
};

export default Pages;
