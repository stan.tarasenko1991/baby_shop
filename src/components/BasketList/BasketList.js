// Modules
import { useEffect, useState } from 'react';

// API
import { fetchAllBasketDevices } from '../../http/userAPI';

// Components
import BasketItem from '../BasketItem/BasketItem';

// Styles
import './basketList-styles.scss';

const BasketList = () => {
  const basketId = sessionStorage.getItem('basketId');
  const [basketDevices, setBasketDevices] = useState([]);
  const [basketCount, setBasketCount] = useState(0);

  useEffect(() => {
    if (basketId) {
      fetchAllBasketDevices(basketId).then(data => {
        setBasketDevices(data.rows)
        setBasketCount(data.count)
      })
    }
  }, [basketId]);

  return (
    <div className='basketList'>
      <div className='basketList_title'>You have {basketCount} devices in your basket</div>
      {basketCount > 0 && basketDevices.map((item) => <BasketItem key={item.id} id={item.deviceId}/>)}
    </div>
  );
};

export default BasketList;
