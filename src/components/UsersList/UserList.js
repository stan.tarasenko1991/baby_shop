// Modules
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button } from 'react-bootstrap';
import { deleteOneUser, fetchUsers } from '../../http/userAPI';
import { setUsers } from '../../store/features/userStore/userSlice';
import { ADMIN_ROLE } from '../../utils/constants';

// Styles
import './userList-styles.scss';

const UsersList = () => {
  const { users } = useSelector((state) => state.user);
  const dispatch = useDispatch();

  const [deleteId, setDeleteId] = useState('');

  const filteredUsers = users.filter(i => i.role !== ADMIN_ROLE);

  const deleteUser = async (id) => {
    setDeleteId(id)
    deleteOneUser(id)
    fetchUsers().then(data => {
      dispatch(setUsers(data.data))
    });
  };

  return (
    <div className='userList_container'>
      <h3 className='userList_title'>List of users</h3>
      <div className='userList_list'> 
      <div className='userList_list_item'>
        <span>Email</span>
        <span></span>
      </div>
      {filteredUsers.map((user, idx) => 
        <div key={user.id} className='userList_list_item' style={{
          background: idx % 2 === 0 ? 'lightgrey' : '', 
          display: user.id === deleteId ? 'none' : ''}}>
          <div className='userList_list_item_email'>
              {user.email}
            </div>
          <div className='userList_list_item_btnBox'>
          <Button variant='danger' className='userList_list_item_remove' onClick={() => deleteUser(user.id)}>
            Delete
          </Button>
          </div>
        </div>
      )}
      </div>
    </div>
  );
};

export default UsersList;
