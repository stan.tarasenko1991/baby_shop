// Modules
import { useDispatch, useSelector } from 'react-redux';
import { Card } from 'react-bootstrap';
import { setSelectedBrand } from '../../store/features/deviceStore/deviceSlice';

// Styles
import './brand-styles.scss';

const BrandBar = () => {
  const devices = useSelector((state) => state.device);
  const { brands, selectedBrand } = devices;
  const dispatch = useDispatch();

  const selectBrand = (brand) => {
    dispatch(setSelectedBrand(brand))
  }

  return (
    <div className="brand-container">
      {brands.map((brand) => {
        return <div key={brand.id}>
          <Card 
          border='none'
          onClick={() => selectBrand(brand)}
          style={{
            padding: '10px', 
            cursor: 'pointer', 
            backgroundImage: brand.id === selectedBrand.id 
              ? 'linear-gradient(to right, red, blue)' 
              : 'linear-gradient(to right, dodgerblue, blue)',
            fontWeight: 'bold',
            color: 'white',
            textShadow: '1px 1px black'
            }}>
            {brand.name}
          </Card>
        </div>
        }
      )}
    </div>
  );
};

export default BrandBar;
