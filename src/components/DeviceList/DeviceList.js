// Modules
import { useSelector } from 'react-redux';
import DeviceItem from '../DeviceItem/DeviceItem';

// Styles
import './deviceList-styles.scss';

const DeviceList = () => {
  const data = useSelector((state) => state.device);
  const { devices } = data;

  return (
    <div className="deviceList_container">
      {devices && devices.map((device) => 
        <DeviceItem key={device.id} device={device} />
      )}
    </div>
  );
};

export default DeviceList;
