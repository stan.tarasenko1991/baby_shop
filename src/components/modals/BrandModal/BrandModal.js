// Modules
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Badge from 'react-bootstrap/Badge';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';
import Dropdown from 'react-bootstrap/Dropdown';

// API
import { createBrand, deleteOneBrand, fetchBrands } from '../../../http/deviceAPI';
import { DELETE_BUTTON_VALUES } from '../../../utils/constants';
import { setBrands } from '../../../store/features/deviceStore/deviceSlice';

const BrandModal = ({ show, onHide }) => {
  const dispatch = useDispatch();
  const [value, setValue] = useState('');
  const [brand, setBrand] = useState({});
  const [radioValue, setRadioValue] = useState('add');
  const [isEmpty, setIsEmpty] = useState(true);
  const [isEmptyBrand, setIsEmptyBrand] = useState(true);

  const data = useSelector(state => state.device);
  const { brands } = data;

  const addBrand = (value) => {
    createBrand({ name: value }).then(data => setValue(''));
    onHide();
  };

  const deleteBrand = (id) => {
    deleteOneBrand(id).then(data => {
      fetchBrands().then(data => {
        dispatch(setBrands(data))
        setBrand({})
      })
    })
    onHide();
  };

  useEffect(() => {
    if (value.length > 2) {
      setIsEmpty(false)
    } else {
      setIsEmpty(true)
    }
  }, [value]);

  useEffect(() => {
    if (brand.id) {
      setIsEmptyBrand(false)
    } else {
      setIsEmptyBrand(true)
    }
  }, [brand]);

  return (
    <Modal
    show={show}
    onHide={onHide}
    size="lg"
    aria-labelledby="contained-modal-title-vcenter"
    centered
  >
    <Modal.Header closeButton>
      <Modal.Title id="contained-modal-title-vcenter">
      {radioValue === 'add' ? 'Add brand' : 'Delete brand'}
      </Modal.Title>
      <ButtonGroup style={{position: 'absolute', left:'74%'}}>
        {DELETE_BUTTON_VALUES.map((radio, idx) => (
          <ToggleButton
            key={idx}
            id={`radio-${idx}`}
            type="radio"
            variant={idx % 2 ? 'outline-secondary' : 'outline-success'}
            name="radio"
            value={radio.value}
            checked={radioValue === radio.value}
            onChange={(e) => setRadioValue(e.currentTarget.value)}
          >
            {radio.name}
          </ToggleButton>
        ))}
      </ButtonGroup>
    </Modal.Header>
    <Modal.Body>
    <Form>
      {radioValue === 'add' 
      ?
        <>
          <Badge 
            bg={isEmpty ? "secondary" : "none"} 
            pill
          >
            Minimum 3 symbols...
          </Badge>
          <Form.Control 
            className='mt-1'
            onChange={e => setValue(e.target.value)}
            placeholder='Add brand name...' 
            value={value}
          />
        </>  
      :   <Dropdown className='mt-1'>
            <Dropdown.Toggle style={{minWidth: '170px'}}>{brand.name || 'Choose the brand'}</Dropdown.Toggle>
            <Dropdown.Menu>
              {brands.map((brand) => 
                <Dropdown.Item key={brand.id} onClick={() => setBrand({name: brand.name, id: brand.id})}>
                  {brand.name}
                </Dropdown.Item>
              )}
            </Dropdown.Menu>
          </Dropdown>
      }
    </Form>  
    </Modal.Body>
    <Modal.Footer>
    {radioValue === 'add' 
      ? <Button disabled={isEmpty} variant='outline-primary' onClick={() => addBrand(value)}>Add brand</Button>
      : <Button disabled={isEmptyBrand} variant='outline-primary' onClick={() => deleteBrand(brand.id)}>Delete brand</Button>
    }  
      <Button variant='outline-danger' onClick={onHide}>Close</Button>
    </Modal.Footer>
  </Modal>
  );
};

export default BrandModal;
