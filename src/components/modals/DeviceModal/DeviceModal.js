// Modules
import { useState } from 'react';
import { useSelector } from 'react-redux';
import Button from 'react-bootstrap/Button';
import Dropdown from 'react-bootstrap/Dropdown';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import { Row, Col } from 'react-bootstrap';
import { createDevice } from '../../../http/deviceAPI';

const DeviceModal = ({ show, onHide }) => {
  const data = useSelector(state => state.device);
  const { types, brands } = data;
  const [name, setName] = useState('');
  const [price, setPrice] = useState(0);
  const [file, setFile] = useState(null);
  const [brand, setBrand] = useState({});
  const [type, setType] = useState({});
  const [info, setInfo] = useState([]);

  const addDevice = () => {
    let formData = new FormData();
    formData.append('name', name)
    formData.append('price', `${price}`)
    formData.append('img', file)
    formData.append('brandId', brand.id)
    formData.append('typeId', type.id)
    formData.append('info', JSON.stringify(info))

    createDevice(formData).then(data => {
      onHide();
    })
  };

  const selectFile = (e) => {
    setFile(e.target.files[0])
  };

  const addInfo = () => {
    setInfo([...info, { title: '', description: '', number: Date.now() }])
  };

  const removeInfo = (number) => {
    setInfo(info.filter(i => i.number !== number))
  };

  const changeInfo = (key, value, number) => {
    setInfo(info.map(i => i.number === number 
      ? {...i, [key]: value}
      : i
    ))
  };

  return (
    <Modal
    show={show}
    onHide={onHide}
    size="lg"
    aria-labelledby="contained-modal-title-vcenter"
    centered
  >
    <Modal.Header closeButton>
      <Modal.Title id="contained-modal-title-vcenter">
        Add device
      </Modal.Title>
    </Modal.Header>
    <Modal.Body>
      <Form>
        <Dropdown className='mt-1'>
          <Dropdown.Toggle style={{minWidth: '170px'}}>{type.name || 'Choose the type'}</Dropdown.Toggle>
          <Dropdown.Menu>
            {types.map((type) => 
              <Dropdown.Item key={type.id} onClick={() => setType({name: type.name, id: type.id})}>
                {type.name}
              </Dropdown.Item>
            )}
          </Dropdown.Menu>
        </Dropdown>
        <Dropdown className='mt-3 mb-3'>
          <Dropdown.Toggle style={{minWidth: '170px'}}>{brand.name || 'Choose the brand'}</Dropdown.Toggle>
          <Dropdown.Menu>
            {brands.map((brand) => 
              <Dropdown.Item key={brand.id} onClick={() => setBrand({name: brand.name, id: brand.id})}>
                {brand.name}
              </Dropdown.Item>
            )}
          </Dropdown.Menu>
        </Dropdown>
        <h5>Add device name</h5>
        <Form.Control className='mt-3 mb-3' onChange={e => setName(e.target.value)} placeholder='Add device name' type='text' value={name} />
        <h5>Add device price</h5>
        <Form.Control className='mt-3 mb-3' onChange={e => setPrice(e.target.value)} placeholder='Add price' type='number' value={price} />
        <h5>Add image file</h5>
        <Form.Control className='mt-3' onChange={selectFile} placeholder='Add image URL' type='file' />
        <Button className='mt-3' onClick={addInfo} variant='outline-dark'>Add description</Button>
        {
          info.map((i) => 
            <Row key={i.number} className='mt-2'>
              <Col md={4}>
                <Form.Control onChange={e => changeInfo('title', e.target.value, i.number)} placeholder='Type the name' value={i.title} />
              </Col>
              <Col md={4}>
                <Form.Control onChange={e => changeInfo('description', e.target.value, i.number)} placeholder='Type the description' value={i.description} />
              </Col>
              <Col md={4}>
                <Button variant='outline-danger' onClick={() => removeInfo(i.number)}>Delete</Button>
              </Col>
            </Row>
          )
        }
      </Form>
    </Modal.Body>
    <Modal.Footer>
      <Button variant='outline-success' onClick={addDevice}>Add</Button>
      <Button variant='outline-danger' onClick={onHide}>Close</Button>
    </Modal.Footer>
  </Modal>
  );
};

export default DeviceModal;
