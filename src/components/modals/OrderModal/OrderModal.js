// Modules
import { useState } from 'react';
import { useDispatch } from 'react-redux';

import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Modal from 'react-bootstrap/Modal';
import { setUserInfo } from '../../../store/features/userStore/userSlice';

const OrderModal = (props) => {
  const dispatch = useDispatch();
  const [lastName, setLastName] = useState('');
  const [firstName, setFirstName] = useState('');
  const [email, setEmail] = useState('');
  const [city, setCity] = useState('');
  const [phone, setPhone] = useState('');
  const [province, setProvince] = useState('');
  const [validated, setValidated] = useState(false);

  const clearForm = () => {
    setLastName('');
    setFirstName('');
    setEmail('');
    setCity('');
    setPhone('');
    setProvince('');
  }

  const handleSubmit = () => {
    if (!!lastName && !!firstName && !!email && !!city && !!phone && !!province) {
      setValidated(false);
      dispatch(setUserInfo({
        name: firstName,
        lastname: lastName,
        userEmail: email,
        userCity: city,
        userPhone: phone,
        userProvince: province
      }));
      clearForm();
    } else {
      setValidated(true);
    }
  };

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          User Detais
        </Modal.Title>
      </Modal.Header>

      <Form validated={validated}>

      <Modal.Body>
      <Row className="mb-3">
        <Form.Group as={Col} md="6" controlId="validationCustom01">
          <Form.Label>First name</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="First name"
            onChange={e => setFirstName(e.target.value)}
            value={firstName}
          />
          <Form.Control.Feedback type="invalid">
            Please provide name.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="6" controlId="validationCustom02">
          <Form.Label>Last name</Form.Label>
          <Form.Control
            required
            type="text"
            placeholder="Last name"
            onChange={e => setLastName(e.target.value)}
            value={lastName}
          />
          <Form.Control.Feedback type="invalid">
            Please provide Last name.
          </Form.Control.Feedback>
        </Form.Group>
      </Row>

      <Row className="mb-3">
        <Form.Group as={Col} md="6" controlId="validationEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control 
            type="text" 
            placeholder="Email" 
            required 
            onChange={e => setEmail(e.target.value)}
            value={email}
          />
          <Form.Control.Feedback type="invalid">
            Please provide a valid email.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="6" controlId="validationPhone">
          <Form.Label>Phone number</Form.Label>
          <Form.Control 
            type="text" 
            placeholder="Phone number" 
            required 
            onChange={e => setPhone(e.target.value)}
            value={phone}
          />
          <Form.Control.Feedback type="invalid">
            Please provide a valid phone number.
          </Form.Control.Feedback>
        </Form.Group>
        </Row>

        <Row className="mb-3">
        <Form.Group as={Col} md="6" controlId="validationCity">
          <Form.Label>City</Form.Label>
          <Form.Control 
            type="text" 
            placeholder="City" 
            required 
            onChange={e => setCity(e.target.value)}
            value={city}
          />
          <Form.Control.Feedback type="invalid">
            Please provide a valid city.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="6" controlId="validationProvince">
          <Form.Label>Province</Form.Label>
          <Form.Control 
            type="text" 
            placeholder="Province" 
            required 
            onChange={e => setProvince(e.target.value)}
            value={province}
          />
          <Form.Control.Feedback type="invalid">
            Please provide a valid province.
          </Form.Control.Feedback>
        </Form.Group>
        </Row>

      {/* <Form.Group className="mb-3">
        <Form.Check
          required
          label="Agree to terms and conditions"
          feedback="You must agree before submitting."
          feedbackType="invalid"
        />
      </Form.Group> */}
      </Modal.Body>

      <Modal.Footer>
        <Button onClick={(e) => handleSubmit(e)}>Next</Button>
      </Modal.Footer>
      </Form>
    </Modal>
  );
};

export default OrderModal;
