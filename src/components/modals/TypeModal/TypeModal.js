// Modules
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Badge from 'react-bootstrap/Badge';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';
import Dropdown from 'react-bootstrap/Dropdown';

// API
import { createType, deleteOneType, fetchTypes } from '../../../http/deviceAPI';
import { setTypes } from '../../../store/features/deviceStore/deviceSlice';
import { DELETE_BUTTON_VALUES } from '../../../utils/constants';

const TypeModal = ({ show, onHide }) => {
  const dispatch = useDispatch();
  const [type, setType] = useState({});
  const [value, setValue] = useState('');
  const [radioValue, setRadioValue] = useState('add');
  const [isEmpty, setIsEmpty] = useState(true);
  const [isEmptyType, setIsEmptyType] = useState(true);
  
  const data = useSelector(state => state.device);
  const { types } = data;

  const addType = (value) => {
    createType({ name: value }).then(data => setValue(''));
    onHide();
  };

  const deleteType = (id) => {
    deleteOneType(id).then(data => {
      fetchTypes().then(data => {
        dispatch(setTypes(data))
        setType({})
      })
    })
    onHide();
  };

  useEffect(() => {
    if (value.length > 2) {
      setIsEmpty(false)
    } else {
      setIsEmpty(true)
    }
  }, [value]);

  useEffect(() => {
    if (type.id) {
      setIsEmptyType(false)
    } else {
      setIsEmptyType(true)
    }
  }, [type]);

  return (
    <Modal
      show={show}
      onHide={onHide}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {radioValue === 'add' ? 'Add type' : 'Delete Type'}
        </Modal.Title>
        <ButtonGroup style={{position: 'absolute', left:'74%'}}>
        {DELETE_BUTTON_VALUES.map((radio, idx) => (
          <ToggleButton
            key={idx}
            id={`radio-${idx}`}
            type="radio"
            variant={idx % 2 ? 'outline-secondary' : 'outline-success'}
            name="radio"
            value={radio.value}
            checked={radioValue === radio.value}
            onChange={(e) => setRadioValue(e.currentTarget.value)}
          >
            {radio.name}
          </ToggleButton>
        ))}
      </ButtonGroup>
      </Modal.Header>
      <Modal.Body>
        <Form>
          {radioValue === 'add' 
          ? <>
              <Badge 
                bg={isEmpty ? "secondary" : "none"} 
                pill
              >
                Minimum 3 symbols...
              </Badge>
              <Form.Control 
                className='mt-2'
                onChange={e => setValue(e.target.value)}
                placeholder='Add type name...' 
                value={value}
              /> 
            </>
          : <Dropdown className='mt-1'>
            <Dropdown.Toggle style={{minWidth: '170px'}}>{type.name || 'Choose the type'}</Dropdown.Toggle>
            <Dropdown.Menu>
              {types.map((type) => 
                <Dropdown.Item key={type.id} onClick={() => setType({name: type.name, id: type.id})}>
                  {type.name}
                </Dropdown.Item>
              )}
            </Dropdown.Menu>
          </Dropdown>}
        </Form>
      </Modal.Body>
      <Modal.Footer>
        {radioValue === 'add' 
          ? <Button disabled={isEmpty} variant='outline-primary' onClick={() => addType(value)}>Add type</Button>
          : <Button disabled={isEmptyType} variant='outline-primary' onClick={() => deleteType(type.id)}>Delete type</Button>
        }
        <Button variant='outline-danger' onClick={onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default TypeModal;
