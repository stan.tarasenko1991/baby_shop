// Modules
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { NavLink } from 'react-router-dom';
import { setIsAuth } from '../../store/features/userStore/userSlice';

// Utils
import { ADMIN_ROLE, ADMIN_ROUTE, BASKET_ROUTE, LOGIN_ROUTE, SHOP_ROUTE, USER_ROLE } from '../../utils/constants';

// Styles
import './navbar-styles.scss';

const NavBar = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);
  const { isAuth } = user;
  const role = sessionStorage.getItem('role');

  const logOut = () => {
    navigate(LOGIN_ROUTE);
    dispatch(setIsAuth(false));
    sessionStorage.clear();
  }

  return (
    <>
      <Navbar> 
        <div className="navbar_main">
          <NavLink to={SHOP_ROUTE} className="main_title">STAN STORE</NavLink>
          {isAuth 
          ? <Nav className="ml-auto">
            {role === ADMIN_ROLE && <div className="button_container">
              <Button variant="outline-light" onClick={() => navigate(ADMIN_ROUTE)}>
                Admin Panel
              </Button>
            </div>}
            {role === USER_ROLE && 
            <>
              <div className="button_container">
                <Button
                  variant="outline-info" 
                  onClick={() => navigate(BASKET_ROUTE)}
                    >Basket
                  </Button>
              </div>
              <div className="button_container">
                <Button style={{marginRight: '10px'}} variant="outline-light" onClick={() => logOut()}>Log Out</Button>
              </div>
            </>
            }
            </Nav> 
          : <Nav className="ml-auto">
              <Button style={{marginRight: '30px'}} variant="outline-light" onClick={() => navigate(LOGIN_ROUTE)}>Log In</Button>
            </Nav>
            }
        </div>
      </Navbar>
    </>
  );
};

export default NavBar;
