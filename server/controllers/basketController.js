const { Basket } = require('../models/models');
const ApiError = require('../error/ApiError');

class BasketController {
  async create(req, res, next) {
    try {
        let {userId} = req.body
        const basket = await Basket.create({userId});

        return res.json(basket)
    } catch (e) {
        next(ApiError.badRequest(e.message))
    }
}

  async getOne(req, res) {
    try {
      const {userId} = req.params
        const basket = await Basket.findOne(
            {
              where: {userId}
            },
        )
        return res.json(basket)
    } catch (e) {
      next(ApiError.badRequest(e.message))
    }
  }

}

module.exports = new BasketController()
