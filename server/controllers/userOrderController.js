const { UserOrderInfo } = require('../models/models')
const ApiError = require('../error/ApiError')

class UserOrderController {
  async create(req, res, next) {
    try {
      let {firstName, lastName, email, phone, city, province} = req.body
      const userOrder = await UserOrderInfo.create({firstName, lastName, email, phone, city, province})
      return res.json(userOrder)
    } catch (e) {
      next(ApiError.badRequest(e.message))
    }
  }
}

module.exports = new UserOrderController()
