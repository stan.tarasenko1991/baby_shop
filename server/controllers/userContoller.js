const ApiError = require('../error/ApiError')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const { User, Basket } = require('../models/models')

const generateJwt = (id, email, role) => {
  return jwt.sign(
    { id, email, role }, 
    process.env.SECRET_KEY, 
    { expiresIn: '24h' }
  )
}

class UserController {
  async registration(req, res, next) {
    const { email, password, role } = req.body
    if (!email || !password) {
      next(ApiError.badRequest('Email and password are require!'))
    }
    const candidate = await User.findOne({ where: { email }})
    if (candidate) {
      next(ApiError.badRequest('This email already in use!'))
    }
    const hashPassword = await bcrypt.hash(password, 5)
    const user = await User.create({ email, role, password: hashPassword })
    const basket = await Basket.create({ userId: user.id })
    const jsonwebtoken = generateJwt(user.id, user.email, user.role)
    return res.json({ jsonwebtoken, basket })  
  }

  async login(req, res, next) {
    const { email, password, userId } = req.body
    const user = await User.findOne({ where: { email }})
    if (!user) {
      return next(ApiError.internal('User not found!'))
    }
    let comparePassword = bcrypt.compareSync(password, user.password)
    if (!comparePassword) {
      return next(ApiError.internal('Password is invalid!'))
    }
    const jsonwebtoken = generateJwt(user.id, user.email, user.role)
    return res.json({ jsonwebtoken })
  }

  async check(req, res) {
    const token = generateJwt(req.user.id, req.user.email, req.user.role)
    return res.json({token})
  }

  async getAll(req, res) {
    const users = await User.findAll()
    return res.json(users)
  }

  async deleteOne(req, res) {
    const {id} = req.params;
    const user = await User.destroy(
      {
        where: {id}
    })
    .then(function (deletedRecord) {
        if(deletedRecord === 1){
            res.status(200).json({message:"Deleted successfully"});          
        }
        else
        {
            res.status(404).json({message:"record not found"})
        }
    })
    .catch(function (error){
        res.status(500).json(error);
    }
  )
  return res.json(user)
}
}

module.exports = new UserController()
