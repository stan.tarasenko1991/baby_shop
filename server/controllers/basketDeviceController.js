const { BasketDevice } = require('../models/models')
const ApiError = require('../error/ApiError')

class BasketDeviceController {
  async create(req, res, next) {
    try {
      let {basketId, deviceId} = req.body
      const basketDevice = await BasketDevice.create({basketId, deviceId})
      return res.json(basketDevice)
    } catch (e) {
      next(ApiError.badRequest(e.message))
    }
  }

  async getAll(req, res) {
    const {basketId} = req.params
    const devicesInBasket = await BasketDevice.findAndCountAll({
      where: {basketId}
  })
    return res.json(devicesInBasket)
  }
}

module.exports = new BasketDeviceController()
