const Router = require('express')
const basketDeviceController = require('../controllers/basketDeviceController')
const router = new Router()

router.post('/', basketDeviceController.create)
router.get('/:basketId', basketDeviceController.getAll)

module.exports = router
