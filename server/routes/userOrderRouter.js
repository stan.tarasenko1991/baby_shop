const Router = require('express')
const router = new Router()
const userOrderController = require('../controllers/userOrderController')

router.post('/', userOrderController.create)

module.exports = router
