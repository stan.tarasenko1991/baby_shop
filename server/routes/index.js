const Router = require('express')
const router = new Router()

const basketRouter = require('./basketRouter')
const basketDeviceRouter = require('./basketDeviceRouter')
const brandRouter = require('./brandRouter')
const deviceRouter = require('./deviceRouter')
const typeRouter = require('./typeRouter')
const userRouter = require('./userRouter')
const userOrderRouter = require('./userOrderRouter')

router.use('/basket', basketRouter)
router.use('/basket_devices', basketDeviceRouter)
router.use('/brand', brandRouter)
router.use('/device', deviceRouter)
router.use('/type', typeRouter)
router.use('/user', userRouter)
router.use('/user_orders', userOrderRouter)

module.exports = router
